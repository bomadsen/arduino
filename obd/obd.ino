#include <BMOBD.h>

BMOBD obd = BMOBD();

void setup()
{
  delay(5000);
  
  while(obd.getConnectionState() != OBD_NO_VEHICLE) {
  Serial.print("Initializing OBD UART...");
  obd.begin(&Serial3);
  if(obd.getConnectionState() != OBD_NO_VEHICLE) {
    Serial.println("ERROR!");
    Serial.print("Connection state:");
    Serial.println(obd.getConnectionState());
    Serial.print("Query state:");
    Serial.println(obd.getQueryState());
    Serial.print("Buffer:");
    Serial.println(obd.getResult());
  } else {
    Serial.println("SUCCESS!");
  }
  }
  
  Serial.print("Connecting OBD...");
  obd.connect();
  if(obd.getConnectionState() != OBD_READY) {
    Serial.println("ERROR!");
    Serial.print("Buffer:");
    Serial.println(obd.getResult());
    while(true) {}
  } else {
    Serial.println("SUCCESS!");
  }
}

char* commands[] = {"ATRV",     "010C", "0105",     "0104",                     "0111",               "010E",           "010F"};
char* desc[] = {    "Voltage",   "RPM", "Coolant",   "Calculated engine load",  "Throttle position",  "Timing advance", "Intake air temperature"};
byte obd_values[3];
uint8_t cmds = 3;

void loop()
{
  for(int i=0; i<cmds; i++)
  {
    uint16_t start = millis();
    Serial.println("----------------------");
    Serial.print(desc[i]);
    Serial.print("...");
    
    obd.queryAndBlock(commands[i]);
    if(obd.getQueryState() != OBD_QUERY_SUCCESS) {
      Serial.print("ERROR ");
      Serial.println(obd.getQueryState());
      Serial.print("Buffer:");
      Serial.println(obd.getResult());
     while(true) {}
    } else {
      Serial.println("SUCCESS!");
    }
    Serial.print("Value:");
    switch(i) {
      case 0:
        Serial.println(obd.getResult());
        break;
      case 1:
        obd.getBytes(obd_values, 2);
        Serial.println(((obd_values[0]*256)+obd_values[1])/4);
        break;
      case 2:
        obd.getBytes(obd_values, 2);
        Serial.println(obd_values[0]-40);
        break;
      case 3:
        obd.getBytes(obd_values, 1);
        Serial.println(obd_values[0]*100/255);
        break;
      case 4:
        obd.getBytes(obd_values, 1);
        Serial.println(obd_values[0]*100/255);
        break;
      case 5:
        obd.getBytes(obd_values, 1);
        Serial.println((obd_values[0]-128)/2);
        break;
      case 6:
        obd.getBytes(obd_values, 1);
        Serial.println(obd_values[0]-40);
        break;
    }
    Serial.print(millis() - start);
    Serial.println("ms");
  }
  delay(1000);
}
