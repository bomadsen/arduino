#include "Arduino.h"
#include "BMGPS.h"

BMGPS::BMGPS()
{
	// noop
}

bool BMGPS::hasFix() { return _gps_fix; }
char* BMGPS::getTime() { return _gps_time; }
uint32_t* BMGPS::getLatitude() { return &_gps_latitude; }
uint32_t* BMGPS::getLongitude() { return &_gps_longitude; }
char* BMGPS::getAltitude() { return _gps_altitude; }
char* BMGPS::getHdop() { return _gps_hdop; }
char* BMGPS::getSpeed() { return _gps_speed; }
char* BMGPS::getHeading() { return _gps_heading; }

bool BMGPS::begin(HardwareSerial *ser)
{
	_serial = ser;
  _serial->begin(9600);
  delay(100);
  _serial->println(F("\r\n"));
//  Serial.print(F("$PMTK251,57600*2C\r\n")); // 57600 baud
  _serial->print(F("$PMTK251,115200*1F\r\n")); // 115200 baud
  _serial->flush();
  _serial->end();
  delay(100);
  
  _serial->begin(115200);
  delay(100);
  _serial->println(F("\r\n"));

//  Serial.print(F("$PMTK220,1000*1F\r\n")); // 1hz
//  Serial.print(F("$PMTK300,1000,0,0,0,0*1C\r\n")); // update each 1000ms
  _serial->print(F("$PMTK220,200*2C\r\n")); // 5hz
  _serial->print(F("$PMTK300,200,0,0,0,0*2F\r\n")); // update each 200ms
  _serial->print(F("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n")); // GPRMC and GGA
//  Serial.print(F("$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n")); // GPRMC
  _serial->print(F("$PGCMD,33,0*6C\r\n")); // no antenna updates
  
  return true;
}

/*
Reads data from the serial line, and pools
it together in a buffer.
When a full valid NMEA is received, parseNmea() is called.
*/
void BMGPS::processSerialData()
{
	while(_serial->available())
	{
		char c = _serial->read();
		if(c == '\n') {
			_gpsBuffer[_gpsBufferIdx] = 0;
			parseNmea(_gpsBuffer);
			_gpsBufferIdx = 0;
		} else {
			if(c == '$') {
				_gpsBuffer[_gpsBufferIdx] = 0;
				_gpsBufferIdx = 0;
			}
			_gpsBuffer[_gpsBufferIdx++] = c;
			while(_gpsBufferIdx >= bmgps_bufferSize) {
				_gpsBufferIdx--;
#if GPSECHO
Serial.println("GPS buffer overrun!!!");
#endif
			}
		}
	}
}
		
/*
Parses the nmea string, and update the gps_xxx
values accordingly.
*/
void BMGPS::parseNmea(const char* nmea)
{
#if GPSECHO
    Serial.println(nmea);
#endif
  if(nmea[0] != '$') {
    Serial.println("1 Dropped bad nmea frame: ");
    Serial.println(nmea);
    return;
  }
  if (nmea[strlen(nmea)-4] == '*') {
    uint16_t sum = parseHex(nmea[strlen(nmea)-3]) * 16;
    sum += parseHex(nmea[strlen(nmea)-2]);
  
    // check checksum 
    for (uint8_t i=1; i < (strlen(nmea)-4); i++) {
      sum ^= nmea[i];
    }
    if (sum != 0) {
      Serial.println("2 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
  }

  if(strncmp(nmea+3, "GGA", 3) == 0) {
    // $GPGGA,time,latitude,N,longitude,E,fix,sats,HDOP,altitude,M,goid,M,last update,checksum
    uint8_t comma_pointers[15];
    uint8_t found = findCommas(comma_pointers, 15, nmea);
    if (found != 14) {
      Serial.println("4 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
    
    copyToken(_gps_time, nmea, comma_pointers, 1, 10);

    copyToken(_gps_latitude_str, nmea, comma_pointers, 2, 14);
    _gps_latitude = parseGeo(_gps_latitude_str, 2);
    
    copyToken(_gps_longitude_str, nmea, comma_pointers, 4, 14);
    _gps_longitude = parseGeo(_gps_longitude_str, 3);
    
    copyToken(_gps_hdop, nmea, comma_pointers, 8, 10);
    copyToken(_gps_altitude, nmea, comma_pointers, 9, 10);
    char fix = nmea[comma_pointers[5]+1];
    _gps_fix = fix == '1' || fix == '2';
  } else if(strncmp(nmea+3, "RMC", 3) == 0) {
    // $GPRMC,UTC,status,latitude,N,longitude,E,speed,couse,UT date,magnetic,E,checksum
    uint8_t comma_pointers[13];
    uint8_t found = findCommas(comma_pointers, 13, nmea);
    if (found != 12) {
      Serial.println("3 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
    copyToken(_gps_speed, nmea, comma_pointers, 7, 10);
    copyToken(_gps_heading, nmea, comma_pointers, 8, 10);
  }
}

// read a Hex value and return the decimal equivalent
uint8_t BMGPS::parseHex(char c) {
    if (c < '0')
      return 0;
    if (c <= '9')
      return c - '0';
    if (c < 'A')
       return 0;
    if (c <= 'F')
       return (c - 'A')+10;
}

uint8_t BMGPS::findCommas(uint8_t* comma_pointers, uint8_t num, const char* str)
{
  uint8_t str_ptr = 0;
  uint8_t comma_ptr = 0;
  do
  {
    if(str[str_ptr] == ',') {
      comma_pointers[comma_ptr++] = str_ptr;
    }
  } while(comma_ptr < num && str[++str_ptr] != 0);
  return comma_ptr;
}

/*
Copies the token numbered by <no> from source into destination.
destination: the destination string
source: the source string
comma_pointers: index of all ',' characters in the source string
no: the number of the token that should be copied (token defined as the text between to comma's)
*/
uint8_t BMGPS::copyToken(char* destination, const char* source, const uint8_t* comma_pointers, const uint8_t no, const int maxLength)
{
  uint8_t startIdx = comma_pointers[no-1] + 1;
  uint8_t endIdx = comma_pointers[no];
  uint8_t length = endIdx-startIdx;
  if(maxLength > -1 && length > maxLength)
    length = maxLength;
  strncpy(destination, source+startIdx, length);
  destination[length] = 0;
  return length;
}

/*
Converts from the NMEA latitude/longitude format to decimal using the following formula:
  nmea = 5700.7093,N,01002.3496,E
  (57*1000000)  + ((007093 * 100)/60) = 57011822
  (010*1000000) + ((023496 * 100)/60) = 10039160
Divde the result by 1.000.000 to get the correct decimal number.
*/
uint32_t BMGPS::parseGeo(char* str, uint8_t prefixChars)
{
  moveDot(str, 2);
  char* dotPlace;
  uint32_t ret = strtol(str, &dotPlace, 10) * 1000000;
  ret += (strtol(++dotPlace, NULL, 10) * 100) / 60;
  return ret;
}

/*
If a '.' is found in str, move it <distance> characters forward.
*/
void BMGPS::moveDot(char* str, const uint8_t distance)
{
  uint8_t idx = 0;
  while(str[idx] != 0 && str[idx] != '.')
    idx++;
  if(str[idx] == 0 || idx < distance)
    return; // either we hit the end without finding a dot, or the dot was too close to the start to move
  for(uint8_t i=0; i<distance; i++)
    str[idx-i] = str[idx-i-1];
  str[idx-distance] = '.';
}

