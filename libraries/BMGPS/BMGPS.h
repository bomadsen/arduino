#ifndef BMGPS_h
#define BMGPS_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdlib.h>

#ifndef GPSECHO
#define GPSECHO false
#endif

const uint8_t bmgps_bufferSize = 80;

class BMGPS
{
	public:
		BMGPS();
		bool begin(HardwareSerial *ser);
		void processSerialData();
		
		bool hasFix();
		char* getTime();
		uint32_t* getLatitude();
		uint32_t* getLongitude();
		char* getAltitude();
		char* getHdop();
		char* getSpeed();
		char* getHeading();
		
	private:
		void parseNmea(const char* nmea);
		uint8_t parseHex(char c);
		uint8_t findCommas(uint8_t* comma_pointers, uint8_t num, const char* str);
		uint8_t copyToken(char* destination, const char* source, const uint8_t* comma_pointers, const uint8_t no, const int maxLength);
		uint32_t parseGeo(char* str, uint8_t prefixChars);
		void moveDot(char* str, const uint8_t distance);
	
		bool _gps_fix = false;
		char _gps_time[11];
		uint32_t _gps_latitude;
		uint32_t _gps_longitude;
		char _gps_altitude[10]; // in meters above sea level
		char _gps_hdop[10]; // in meters
		char _gps_speed[10]; // in knots
		char _gps_heading[10]; // in degrees

		// buffer used to read gps NMEA lines
		char _gpsBuffer[bmgps_bufferSize];
		uint8_t _gpsBufferIdx = 0;
		char _gps_latitude_str[16];
		char _gps_longitude_str[16];

		HardwareSerial* _serial;
	};

#endif