#include "Arduino.h"
#include "BMOBD.h"

BMOBD::BMOBD()
{
	// noop
}

void BMOBD::begin(HardwareSerial *ser)
{
	_ser = ser;
	
	_ser->begin(9600);
	_ser->print("\r");
	queryAndBlock("ATZ");

	if(_queryState != OBD_QUERY_SUCCESS)
		return; // we are done
		
	_conState = OBD_NO_VEHICLE;
	// TODO raise baud
}

void BMOBD::connect()
{
	if(_conState == OBD_NOT_CONNECTED)
		return;
		
	queryAndBlock("ATSP0");
	
	if(_queryState != OBD_QUERY_SUCCESS)
		return; // we are done
		
	_conState = OBD_READY;
}

void BMOBD::setTimeout(const char* timeout)
{
	if(_conState == OBD_NOT_CONNECTED)
		return;

	sprintf(_requestBuffer, "AT ST %s", timeout);

	queryAndBlock(_requestBuffer);
}

void BMOBD::query(const char* cmd)
{
	if(_queryState == OBD_QUERY_WAITING)
		return;
	
	_responsePtr = 0;
	_responseBuffer[0] = 0;
	_ser->clear();
	_ser->print(cmd);
	_ser->print('\r');
	_queryState = OBD_QUERY_WAITING;
}

void BMOBD::queryAndBlock(const char* cmd)
{
	_requestStart = millis();
	query(cmd);
	while(_queryState == OBD_QUERY_WAITING) {
		if((millis() - _requestStart) > 10000) {
			_queryState = OBD_QUERY_TIMEOUT;
			return;
		}
		update(); // block until there is a result
	}
}

void BMOBD::update()
{
	while(_ser->available())
	{
		char c = _ser->read();
		if(_queryState != OBD_QUERY_WAITING)
			continue; // eat input bytes even if we are not listening
		if(c == '>') {
			_responseBuffer[_responsePtr++] = 0;
			_queryState = OBD_QUERY_SUCCESS;
		} else {
			_responseBuffer[_responsePtr++] = c;
		}
	}
}

char* BMOBD::getResult()
{
	_queryState = OBD_QUERY_READY;
	return _responseBuffer;
}

void BMOBD::getBytes(uint8_t *values, uint8_t numValues)
{
	_queryState = OBD_QUERY_READY;
	// For each byte expected, package it up
    char hexVal[]="0x00";
	uint8_t i=0;
	for (int i=0; i<numValues; i++){
			hexVal[2]=_responseBuffer[6+(3*i)];
			hexVal[3]=_responseBuffer[7+(3*i)];
			values[i]=strtol(hexVal,NULL,16);
	}
}

