#ifndef BMOBD_h
#define BMOBD_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdlib.h>

// ------------------------ connection states ------------------------
const uint8_t OBD_NOT_CONNECTED = 1; // just started, begin hasn't been called yet
const uint8_t OBD_NO_VEHICLE  = 2; // connected to the UART, but not connected to a vehicle
const uint8_t OBD_READY  = 3; // ready and waiting for a command

// ------------------------ query states ------------------------
const uint8_t OBD_QUERY_READY    = 1; // the OBD is ready for a query
const uint8_t OBD_QUERY_WAITING  = 2; // waiting for a query response
const uint8_t OBD_QUERY_SUCCESS  = 3; // the query was a success
const uint8_t OBD_QUERY_TIMEOUT  = 4; // the query timed out before a response was read
const uint8_t OBD_QUERY_ERROR    = 5; // the query encountered an error

const uint8_t OBD_RES_BUFFER_SIZE = 64;

class BMOBD
{
	public:
		BMOBD();

		/*
		Initializes the connection to the OBD UART.
		*/
		void begin(HardwareSerial *ser);
		
		/*
		Connect the OBD UART to the vehicle
		Available in OBD_NO_VEHICLE state.
		*/
		void connect();
		
		/*
		Return the state of the OBD connection
		*/
		uint8_t getConnectionState() { return _conState; }
		
		/*
		Sets the OBD query time out in milliseconds/4 (so 0A == 40, FF == 1000 which is max), default is 200.
		Available in OBD_READY state.
		*/
		void setTimeout(const char* timeout);
		
		/*
		Sends a command to the OBD chip and waits  for a response.
		Available in OBD_READY state.
		*/
		void query(const char* cmd);
		void queryAndBlock(const char* cmd);
		
		/*
		Updates the OBD state based on input from the UART.
		*/
		void update();
		
		/*
		Return the current query state.
		*/
		uint8_t getQueryState() { return _queryState; }
		
		/*
		Available if the query state is OBD_QUERY_SUCCESS.
		*/
		char* getResult();

		/*
		Available if the query state is OBD_QUERY_SUCCESS.
		*/
		void getBytes(uint8_t *values, uint8_t numValue);
	private:
		HardwareSerial *_ser;
		uint8_t _conState = OBD_NOT_CONNECTED;
		uint8_t _queryState = OBD_QUERY_WAITING;
		
		uint32_t _requestStart;
		uint8_t _responsePtr;
		char _responseBuffer[OBD_RES_BUFFER_SIZE];

		char _requestBuffer[32];
};


#endif
