#ifndef BMAccel_h
#define BMAccel_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdlib.h>

//X current:493 g:-0.210 min:390 max:595
//Y current:499 g:-0.153 min:396 max:595

const uint8_t BMAccel_numSmoothReadings = 10;
const uint8_t BMAccel_numAxis = 2;
const uint8_t BMAccel_axisPins[] = {22, 21}; // x=22, y=21, z=23
const uint16_t BMAccel_minAxisValue = 405;
const uint16_t BMAccel_maxAxisValue = 595;
const uint16_t BMAccel_midAxisValue = 500;

class BMAccel
{
	public:
		BMAccel();

		void begin();
		void updateSamplings();
		uint16_t getAxisValue(uint8_t axis);
		uint16_t getNonCorrectedAxisValue(uint8_t axis);
		
		uint16_t getMinAxisValue() { return BMAccel_minAxisValue; }
		uint16_t getMaxAxisValue() { return BMAccel_maxAxisValue; }
	private:
		uint16_t _axisReadings[BMAccel_numAxis][BMAccel_numSmoothReadings];
		uint8_t _axisIndex[BMAccel_numAxis];
		uint16_t _axisTotal[BMAccel_numAxis];
		int16_t _axisCorrection[BMAccel_numAxis];
	};

#endif