#include "Arduino.h"
#include "BMAccel.h"

BMAccel::BMAccel()
{
}

void BMAccel::begin()
{
	// Initialize data structures
	for(byte axis=0; axis<BMAccel_numAxis; axis++) {
		for(byte r=0; r<BMAccel_numSmoothReadings; r++) {
			_axisReadings[axis][r] = 0;
		}
		_axisIndex[axis] = 0;
		_axisTotal[axis] = 0;
	}

	// Fill the buffers
	for(uint16_t r=0; r<2000; r++) {
		updateSamplings();
	}

	// Calculate corrections
	for(byte axis=0; axis<BMAccel_numAxis; axis++) {
//		Serial.print("Axis ");
//		Serial.print(axis);
		uint16_t value = getNonCorrectedAxisValue(axis);
//		Serial.print(" value: ");
//		Serial.print(value);
		_axisCorrection[axis] = (BMAccel_midAxisValue - value) * BMAccel_numSmoothReadings;
//		Serial.print(" correction: ");
//		Serial.println(_axisCorrection[axis]);
	}
}

void BMAccel::updateSamplings()
{
	for(byte axis=0; axis<BMAccel_numAxis; axis++) {
		_axisIndex[axis]++;
		if(_axisIndex[axis] >= BMAccel_numSmoothReadings)
			_axisIndex[axis] = 0;

		_axisTotal[axis] -= _axisReadings[axis][_axisIndex[axis]];
		_axisReadings[axis][_axisIndex[axis]] = analogRead(BMAccel_axisPins[axis]);
		_axisTotal[axis] += _axisReadings[axis][_axisIndex[axis]];
	}
}

uint16_t BMAccel::getAxisValue(uint8_t axis)
{
	return (_axisTotal[axis] + _axisCorrection[axis]) / BMAccel_numSmoothReadings;
}

uint16_t BMAccel::getNonCorrectedAxisValue(uint8_t axis)
{
	return _axisTotal[axis] / BMAccel_numSmoothReadings;
}
