#include "Arduino.h"
#include "BMButton.h"

BMButton::BMButton()
{
}

void BMButton::begin(const uint8_t pin, const uint8_t debounceTime)
{
	_pin = pin;
	_debounceTime = debounceTime;
	_lastButtonState = LOW;
	_lastButtonChangeMillies = millis();
	_clicks = 0;
	
	pinMode(_pin, INPUT);
	digitalWrite(_pin, LOW);
}

void BMButton::update()
{
  long currentMillies = millis();
  
  if((currentMillies - _lastButtonChangeMillies) < _debounceTime) {
    return; // ignore changes during the debounce period
  }
  
  int currentState = digitalRead(_pin);

  // button is pressed
  if(currentState == HIGH && _lastButtonState == LOW) {
    _lastButtonChangeMillies = currentMillies;
    _lastButtonState = HIGH;
    _clicks = 1;
  } else if(currentState == LOW && _lastButtonState == HIGH) {
    _lastButtonChangeMillies = currentMillies;
    _lastButtonState = LOW;
  }

}

bool BMButton::wasButtonPressed()
{
  bool ret = _clicks == 1;
  _clicks = 0;
  return ret;
}

