#ifndef BMButton_h
#define BMButton_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdlib.h>

class BMButton
{
	public:
		BMButton();

		void begin(const uint8_t pin, const uint8_t debounceTime);
		void update();
		bool wasButtonPressed();
	private:
		uint8_t _pin; 
		uint8_t _debounceTime; 
		uint8_t _lastButtonState = LOW;
		uint16_t _lastButtonChangeMillies;
		uint8_t _clicks = 0;
	};


#endif