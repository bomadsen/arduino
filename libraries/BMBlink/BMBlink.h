#ifndef BMBlink_h
#define BMBlink_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <stdlib.h>
#include <Metro.h>

class BMBlink
{
	public:
		BMBlink();

		void begin(uint8_t pins[], uint8_t no);
		
		/*
		Clears all diods, and stops all blinking
		*/
		void clear();
		
		/*
		Updates any blinking state.
		*/
		void update();

		void flash(const uint8_t idx, const uint16_t milis);
		bool isFlashing(const uint8_t idx);
		void on(const uint8_t idx);
		void off(const uint8_t idx);
		
	private:
		uint8_t* _pins; 
		uint8_t _no; 
		int8_t _blinkIdx = -1;
		uint8_t _blinkState = 0;
		Metro _blinkTickMetro = Metro(200);
	};


#endif