#include "Arduino.h"
#include "BMBlink.h"

BMBlink::BMBlink()
{
}

void BMBlink::begin(uint8_t pins[], uint8_t no)
{
	_pins = pins;
	_no = no;
	
	// Set all pins to output
	for(uint8_t i=0; i<_no; i++) {
		pinMode(_pins[i], OUTPUT);
		on(i);
	}
}

/*
Clears all diods, and stops all blinking
*/
void BMBlink::clear()
{
	_blinkState = 0;
	_blinkIdx = -1;

	for(uint8_t i=0; i<_no; i++) {
		off(i);
	}
}

/*
Updates any blinking state.
*/
void BMBlink::update()
{
  if(_blinkIdx < 0) {
    return;
  }
  if(_blinkTickMetro.check()) {
    _blinkState = !_blinkState;
    digitalWrite(_pins[_blinkIdx], _blinkState);
  }
}

void BMBlink::flash(const uint8_t idx, const uint16_t milis)
{
	_blinkTickMetro.interval(milis);
	_blinkTickMetro.reset();
	_blinkIdx = idx;
	_blinkState = 1;
	on(_blinkIdx);
}

bool BMBlink::isFlashing(const uint8_t idx)
{
	return _blinkIdx == idx;
}

void BMBlink::on(const uint8_t idx)
{
    digitalWrite(_pins[idx], HIGH);
}

void BMBlink::off(const uint8_t idx)
{
    digitalWrite(_pins[idx], LOW);
}
