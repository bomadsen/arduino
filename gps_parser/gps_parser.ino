
// buffer used to read gps NMEA lines
const byte bufferSize = 80;
char gpsBuffer[bufferSize];
byte gpsBufferIdx = 0;

// gps info
bool gps_fix = false;
char gps_time[11];
char gps_latitude_str[16];
char gps_longitude_str[16];
unsigned long gps_latitude;
unsigned long gps_longitude;
char gps_altitude[10]; // in meters above sea level
char gps_hdop[10]; // in meters
char gps_speed[10]; // in knots
char gps_heading[10]; // in degrees

void setup()
{
  Serial.begin(115200);
  if(!setupSerialGps()) {
    while(true);
  }
}

uint32_t timer = millis();
void loop()
{
  processSerialGps();

  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())  timer = millis();

  // approximately every 1000ms or so, print out the current stats
  if (millis() - timer > 1000) { 
    Serial.println("====================================");
    if(gps_fix) {
      Serial.print("Latitude: ");
      Serial.println(gps_latitude);
  
      Serial.print("Longitude: ");
      Serial.println(gps_longitude);
  
      Serial.print("HDOP: ");
      Serial.println(gps_hdop);
  
      Serial.print("Altitude: ");
      Serial.println(gps_altitude);
  
      Serial.print("Speed: ");
      Serial.println(gps_speed);
  
      Serial.print("Heading: ");
      Serial.println(gps_heading);
    } else {
      Serial.println("No gps fix!");
    }
    timer = millis(); // reset the timer
  }

}


bool setupSerialGps()
{
  Serial1.begin(9600);
  delay(100);
  Serial1.println(F("\r\n"));
//  Serial.print(F("$PMTK251,57600*2C\r\n")); // 57600 baud
  Serial1.print(F("$PMTK251,115200*1F\r\n")); // 115200 baud
  Serial1.flush();
  Serial1.end();
  delay(100);
  
  Serial1.begin(115200);
  delay(100);
  Serial1.println(F("\r\n"));

//  Serial.print(F("$PMTK220,1000*1F\r\n")); // 1hz
//  Serial.print(F("$PMTK300,1000,0,0,0,0*1C\r\n")); // update each 1000ms
  Serial1.print(F("$PMTK220,200*2C\r\n")); // 5hz
  Serial1.print(F("$PMTK300,200,0,0,0,0*2F\r\n")); // update each 200ms
  Serial1.print(F("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n")); // GPRMC and GGA
//  Serial.print(F("$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n")); // GPRMC
  Serial1.print(F("$PGCMD,33,0*6C\r\n")); // no antenna updates
  return true;
}

/*
Reads data from the serial line, and pools
it together in a buffer.
When a full valid NMEA is received, parseNmea() is called.
*/
void processSerialGps()
{
  while(Serial1.available())
  {
    char c = Serial1.read();
    if(c == '\n') {
      gpsBuffer[gpsBufferIdx] = 0;
      parseNmea(gpsBuffer);
      gpsBufferIdx = 0;
    } else {
      if(c == '$') {
        gpsBuffer[gpsBufferIdx] = 0;
        gpsBufferIdx = 0;
      }
      gpsBuffer[gpsBufferIdx++] = c;
      while(gpsBufferIdx >= bufferSize) {
        gpsBufferIdx--;
      }
    }
  }
}

/*
Parses the nmea string, and update the gps_xxx
values accordingly.
*/
void parseNmea(const char* nmea)
{
//  Serial.println(nmea);
  unsigned long start = micros();
  if(nmea[0] != '$') {
    Serial.println("1 Dropped bad nmea frame: ");
    Serial.println(nmea);
    return;
  }
  if (nmea[strlen(nmea)-4] == '*') {
    uint16_t sum = parseHex(nmea[strlen(nmea)-3]) * 16;
    sum += parseHex(nmea[strlen(nmea)-2]);
  
    // check checksum 
    for (uint8_t i=1; i < (strlen(nmea)-4); i++) {
      sum ^= nmea[i];
    }
    if (sum != 0) {
      Serial.println("2 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
  }

  if(strncmp(nmea+3, "GGA", 3) == 0) {
    // $GPGGA,time,latitude,N,longitude,E,fix,sats,HDOP,altitude,M,goid,M,last update,checksum
    byte comma_pointers[15];
    byte found = findCommas(comma_pointers, 15, nmea);
    if (found != 14) {
      Serial.println("4 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
    
    copyToken(gps_time, nmea, comma_pointers, 1, 10);

    copyToken(gps_latitude_str, nmea, comma_pointers, 2, 14);
    gps_latitude = parseGeo(gps_latitude_str, 2);
    
    copyToken(gps_longitude_str, nmea, comma_pointers, 4, 14);
    gps_longitude = parseGeo(gps_longitude_str, 3);
    
    copyToken(gps_hdop, nmea, comma_pointers, 8, 10);
    copyToken(gps_altitude, nmea, comma_pointers, 9, 10);
    char fix = nmea[comma_pointers[5]+1];
    gps_fix = fix == '1' || fix == '2';
  } else if(strncmp(nmea+3, "RMC", 3) == 0) {
    // $GPRMC,UTC,status,latitude,N,longitude,E,speed,couse,UT date,magnetic,E,checksum
    byte comma_pointers[13];
    byte found = findCommas(comma_pointers, 13, nmea);
    if (found != 12) {
      Serial.println("3 Dropped bad nmea frame: ");
      Serial.println(nmea);
      return;
    }
    copyToken(gps_speed, nmea, comma_pointers, 7, 10);
    copyToken(gps_heading, nmea, comma_pointers, 8, 10);
  }
}

// read a Hex value and return the decimal equivalent
uint8_t parseHex(char c) {
    if (c < '0')
      return 0;
    if (c <= '9')
      return c - '0';
    if (c < 'A')
       return 0;
    if (c <= 'F')
       return (c - 'A')+10;
}

byte findCommas(byte* comma_pointers, byte num, const char* str)
{
  byte str_ptr = 0;
  byte comma_ptr = 0;
  do
  {
    if(str[str_ptr] == ',') {
      comma_pointers[comma_ptr++] = str_ptr;
    }
  } while(comma_ptr < num && str[++str_ptr] != 0);
  return comma_ptr;
}

/*
Copies the token numbered by <no> from source into destination.
destination: the destination string
source: the source string
comma_pointers: index of all ',' characters in the source string
no: the number of the token that should be copied (token defined as the text between to comma's)
*/
byte copyToken(char* destination, const char* source, const byte* comma_pointers, const byte no, const int maxLength)
{
  byte startIdx = comma_pointers[no-1] + 1;
  byte endIdx = comma_pointers[no];
  byte length = endIdx-startIdx;
  if(maxLength > -1 && length > maxLength)
    length = maxLength;
  strncpy(destination, source+startIdx, length);
  destination[length] = 0;
  return length;
}

/*
Converts from the NMEA latitude/longitude format to decimal using the following formula:
  nmea = 5700.7093,N,01002.3496,E
  (57*1000000)  + ((007093 * 100)/60) = 57011822
  (010*1000000) + ((023496 * 100)/60) = 10039160
Divde the result by 1.000.000 to get the correct decimal number.
*/
unsigned long parseGeo(char* str, byte prefixChars)
{
  moveDot(str, 2);
  char* dotPlace;
  unsigned long ret = strtol(str, &dotPlace, 10) * 1000000;
  ret += (strtol(++dotPlace, NULL, 10) * 100) / 60;
  return ret;
}

/*
If a '.' is found in str, move it <distance> characters forward.
*/
void moveDot(char* str, const byte distance)
{
  byte idx = 0;
  while(str[idx] != 0 && str[idx] != '.')
    idx++;
  if(str[idx] == 0 || idx < distance)
    return; // either we hit the end without finding a dot, or the dot was too close to the start to move
  for(byte i=0; i<distance; i++)
    str[idx-i] = str[idx-i-1];
  str[idx-distance] = '.';
}

