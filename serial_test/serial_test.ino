//#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>

// setup gps serial
int gpsTxPin = 8;
int gpsRxPin = 9;
//SoftwareSerial gpsSerial(gpsTxPin, gpsRxPin);
//AltSoftSerial gpsSerial;

HardwareSerial gpsSerial = Serial1;

void setup()
{
  Serial.begin(115200);
  
  gpsSerial.begin(9600);
  Serial.println("Ready!");
}

void loop()
{
  while(gpsSerial.available())
  {
    char c = gpsSerial.read();
    Serial.print(c);
  }
}

