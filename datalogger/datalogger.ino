#include <SD.h>
#include <Metro.h>
#include <BMGPS.h>
#include <BMAccel.h>
#include <BMButton.h>
#include <BMBlink.h>

BMGPS gps = BMGPS();
BMAccel accell = BMAccel();
BMButton button = BMButton();
BMBlink leds = BMBlink();

// buffer used to write CSV lines to the SD card
const byte csv_buffer_size=100;
char csv_line[csv_buffer_size];

// Setup SD
const int sdChipSelectPin = 10;
char file_name[16]; // 8.3\0

// IO
const uint8_t blue_pin = 16;
const uint8_t green_pin = 17;
const uint8_t red_pin = 18;
uint8_t led_pins[] = {red_pin, green_pin, blue_pin};
const uint8_t buttonPin = 19; 
const byte accl_x = 0;
const byte accl_y = 1;

// Debug
#define SDECHO false

// ------------------------------------ Setup and loop ------------------------------------

void setup()
{
  // Setup output
  leds.begin(led_pins, 3);
  button.begin(buttonPin, 100);
  delay(500);
  
  // Setup SD
  leds.clear();
  pinMode(10, OUTPUT); // default chip select pin needs to be set to output (don't know why, but the example says so)
  if (!SD.begin(sdChipSelectPin)) {
    signalSDFail();
  }
  
  // Setup acellerometer
  accell.begin();  

  // Setup GPS input
  delay(1000);
  if(!gps.begin(&Serial1)) {
    leds.flash(0, 1000);
    while(true) leds.update();
  }
  
  initializeState();
}

void loop()
{
  leds.update();
  gps.processSerialData();
  button.update();
  accell.updateSamplings();
  updateState();
}

// ------------------------------------ States ------------------------------------

// States
const uint8_t STATE_IDLE = 0;
const uint8_t STATE_NO_GPS_FIX = 1;
const uint8_t STATE_RECORDING = 2;

class State
{
  public:
  virtual void enter(uint8_t previousState) {}
  virtual uint8_t execute() { return 0; }
  virtual void leave(uint8_t nextState) {}
};

class StateIdle : public State
{
  public:
  void enter(uint8_t previousState) {
    leds.on(1);
    button.wasButtonPressed(); // just to clear button state
  }
  uint8_t execute() {
    if(button.wasButtonPressed()) {
      return STATE_RECORDING;
    }
    if(!gps.hasFix()) {
      return STATE_NO_GPS_FIX;
    }
    return STATE_IDLE;
  }
  void leave(uint8_t nextState) {
    leds.clear();
  }
};

class StateNoGpsFix : public State
{
  public:
  void enter(uint8_t previousState) {
    returnState = previousState;
    leds.clear();
    leds.flash(0, 500);
  }
  uint8_t execute() {
    return gps.hasFix() ? returnState : STATE_NO_GPS_FIX;
  }
  void leave(uint8_t nextState) {
    leds.clear();
  }
  
  private:
  uint8_t returnState;
};

class StateRecording : public State
{
  public:
  void enter(uint8_t previousState) {
    leds.flash(2, 200);
    startRecording();
  }
  uint8_t execute() {
    if(button.wasButtonPressed() || !gps.hasFix()) {
      return gps.hasFix() ? STATE_IDLE : STATE_NO_GPS_FIX;
    } else {
      // approximately every 200ms or so, print out the current stats
      if (loopTickMetro.check() == 1) { 
        outputLine();
      }
    }
    return STATE_RECORDING;
  }
  void leave(uint8_t nextState) {
    leds.clear();
  }
  
  private:
  Metro loopTickMetro = Metro(200);
};

State* states[3] = {new StateIdle(), new StateNoGpsFix(), new StateRecording()};
uint8_t currentState = STATE_IDLE;

void initializeState()
{
  states[currentState]->enter(STATE_IDLE);
}

void updateState()
{
  uint8_t newState = states[currentState]->execute();
  if(newState != currentState)
  {
    states[currentState]->leave(newState);
    states[newState]->enter(currentState);
    currentState = newState;
  }
}

// ------------------------------------ CSV creation ------------------------------------

/*
Logfile output format(CSV):
Time	        Sample time in seconds. The first sample does not have to be zero. This may need to be the first column in the file.
GPS Time	Alternate: Sample time in milliseconds. The first sample does not have to be zero.
Latitude	Vehicle position latitude (Y). Can be feet, meters, or coordinates, as long as it's in decimal notation.
Longitude	Vehicle position longitude (X). Can be feet, meters, or coordinates, as long as it's in decimal notation.
Altitude	Vehicle position altitude (Z)
X	        Acceleration / Deceleration (X) G-force
Y	        Lateral (Y) G-force
*MPH	        Vehicle speed in Miles Per Hour (MPH)
KPH	        Alternate: Vehicle speed in Kilometers Per Hour (km/hr)
*Speed (m/s)	Alternate: Vehicle speed in Meters Per Second (m/s)
Heading	        Vehicle heading (degrees; 0 = North)
*Lap	        Current lap number. This only needs to be set once per lap. If not available, you can also manually set the Start / Finish point.
RPM	        Engine speed in Revolutions Per Minute (RPM)
*Gear	        Current transmission gear number (0 for Neutral, -1 for Reverse, -99 for Park)
Throttle	Throttle position (0 to 100)
**Accuracy	GPS position accuracy (meters); used for loggers with low or inconsistent accuracy (such as smartphones)

* = omitted
** = require GGA string
*/
void startRecording()
{
  // set file name
  file_name[0]='D';
  file_name[1]='L';
  strncpy(file_name+2, gps.getTime(), 6);
  file_name[8]='.';
  file_name[9]='c';
  file_name[10]='s';
  file_name[11]='v';
  file_name[12]=0;
  
  // outpute header
  sdwrite("Time,GPS Time,Latitude,Longitude,X,Y,Altitude,KPH,Heading,RPM,Throttle,Accuracy");
}

/*
Compose a CSV line and writes it to the SD card.
*/
void outputLine() 
{
  // reset line
  char* ptr = csv_line;
  
  // Time
  ptr += snprintf(ptr, csv_buffer_size, "%lu", millis());
  *ptr++ = ',';

  // GPS time
  ptr = append(ptr, gps.getTime());

  // Latitude
  ptr = append(ptr, gps.getLatitude());

  // Longitude
  ptr = append(ptr, gps.getLongitude());

  // X G's
  ptr = appendG(ptr, accl_x);
  
  // Y G's
  ptr = appendG(ptr, accl_y);
  
  // Altitude
  ptr = append(ptr, gps.getAltitude());

  // Knots
  ptr = append(ptr, gps.getSpeed());

  // Heading
  ptr = append(ptr, gps.getHeading());

  // RPM
  ptr = append(ptr, "0");

  // Throttle
  ptr = append(ptr, "0");

  // Accuracy
  ptr = append(ptr, gps.getHdop());
  
  *--ptr = 0;
  sdwrite(csv_line);
}

char* append(char* dest, const char* src)
{
  size_t len = strlen(src);
  strncpy(dest, src, len);
  dest+=len;
  *dest++ = ',';
  return dest;
}

char* appendG(char* dest, const int axis)
{
  int current = accell.getAxisValue(axis);
  long g = map(current, accell.getMinAxisValue(), accell.getMaxAxisValue(), 1000, -1000);
  if(g < 0) *dest++ = ('-');
  ltoa(abs(g / 1000), dest, 10);
  while(*++dest != 0);
  *dest++ = ('.');
  sprintf(dest, "%03u", abs(g % 1000));
  while(*++dest != 0);
  *dest++ = ',';
  return dest;
}


char* append(char* dest, const uint32_t* val)
{
  char buffer[10];
  sprintf(buffer, "%lu", *val);
  int len = strlen(buffer);
  
  int header = len-6;
  strncpy(dest, buffer, header);
  dest+=header;
  *dest = '.';
  strncpy(++dest, buffer+header, 6);
  dest+=6;
  *dest = ',';
  return ++dest;
}

void timing(const char* str, const unsigned long time) {
//  File dataFile = SD.open("timing", FILE_WRITE);
//  if (dataFile) {
//    dataFile.print(millis());
//    dataFile.print(": ");
//    dataFile.print(str);
//    dataFile.print(" ");
//    dataFile.print(time);
//    dataFile.close();
//  } else {
//    signalSDFail();
//  } 
//  Serial.println(str);
}


void sdwrite(char* str)
{
#if SDECHO
    Serial.println(str);
#endif
  File dataFile = SD.open(file_name, FILE_WRITE);
  if (dataFile) {
    dataFile.println(str);
    dataFile.close();
  } else {
    signalSDFail();
  } 
}

void signalSDFail()
{
  leds.flash(0, 200);
  while(true) leds.update();
}

