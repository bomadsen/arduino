#include <BMBlink.h>
#include <BMButton.h>
#include <Metro.h>

BMBlink leds = BMBlink();
BMButton button = BMButton();

const uint8_t pins = 3;
const uint8_t blue_pin = 16;
const uint8_t green_pin = 17;
const uint8_t red_pin = 18;
uint8_t led_pins[] = {red_pin, green_pin, blue_pin};
const uint8_t buttonPin = 19; 

uint8_t current_pin = 0;

void setup() {
  delay(2000);
  Serial.begin(9600);
  Serial.println("Initializing...");
  leds.begin(led_pins, pins);
  button.begin(buttonPin, 100);
  leds.flash(current_pin, 200);
  
}

void loop() {
  leds.update();
  button.update();

  if(button.wasButtonPressed()) {
    leds.clear();
    if(++current_pin >= pins)
      current_pin = 0;
    leds.flash(current_pin, 200);
    Serial.print("Flahsing ");
    Serial.println(current_pin);
  }

}
