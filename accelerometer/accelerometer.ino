#include <BMAccel.h>
#include <Metro.h>

BMAccel accell = BMAccel();
Metro loopTickMetro = Metro(500);

char names[] = {'X', 'Y', 'Z'};
uint16_t axisMaxReadings[BMAccel_numAxis];
uint16_t axisMinReadings[BMAccel_numAxis];

void setup()
{
  Serial.begin(115200);
  accell.begin();

  for(int i=0; i<BMAccel_numAxis; i++) {
    axisMaxReadings[i] = 0;
    axisMinReadings[i] = 1024;  
  }
  loopTickMetro.reset();
}

void loop()
{
  accell.updateSamplings();

  bool tick = loopTickMetro.check() == 1;

  if(tick)
    Serial.println("-------------------------------");
    
  for(int i=0; i<BMAccel_numAxis; i++) {
    int noncorrected = accell.getNonCorrectedAxisValue(i);
    int current = accell.getAxisValue(i);
    if(noncorrected > axisMaxReadings[i])
      axisMaxReadings[i] = noncorrected;
    if(noncorrected < axisMinReadings[i])
      axisMinReadings[i] = noncorrected;
    if(tick) {
      Serial.print(names[i]);
      Serial.print(" current:");
      Serial.print(current);
      long g = map(current, accell.getMinAxisValue(), accell.getMaxAxisValue(), -1000, 1000);
      Serial.print(" g:");

      if(g < 0) Serial.print('-');
      Serial.print(abs(g / 1000));
      Serial.print(".");
  
      char buffer[10];
      sprintf(buffer, "%03u", abs(g % 1000));
      Serial.print(buffer);
      Serial.print(" (");
      Serial.print(g);
      Serial.print(")");

      Serial.print(" noncorrected:");
      Serial.print(noncorrected);

      Serial.print(" min:");
      Serial.print(axisMinReadings[i]);
      Serial.print(" max:");
      Serial.print(axisMaxReadings[i]);

      Serial.println("");
    }

  }
}


